@extends('layout.master')

@section('judul')
Dashboard
@endsection

@section('sub-judul')
Media Online
@endsection

@section('content')
<h3>Sosial Media Developer</h3>
<p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
<h3>Benefit Join di Media Online</h3>
<ul>
    <li>Mendapatan motivasi dari sesama para Developer</li>
    <li>Sharing knowledge</li>
    <li>Dibuat oleh calon web developer terbaik</li>
</ul>
<h3>Cara Bergabung ke Media Online</h3>
<ol>
    <li>Mengunjungi Website ini</li>
    <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
    <li>Selesai</li>
    </ol>
@endsection