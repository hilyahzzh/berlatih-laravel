@extends('layout.master')

@section('judul')
Halaman Deatil Cast {{$cast->nama}}
@endsection

@section('sub-judul')
Halaman Detail Cast {{$cast->nama}}
@endsection

@section('content')
<h3>{{$cast->nama}}</h3>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>
@endsection