<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio(){
        return view('halaman.form');
    }
    public function submit(Request $request){  
        $fname = $request->first_name;
        $lname = $request->last_name;
        return view('halaman.welcome', compact('fname','lname'));
    }
}
